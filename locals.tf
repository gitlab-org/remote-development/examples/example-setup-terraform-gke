locals {
  # we need to make sure the resulting domain name (derived from normalized_env_prefix) is not larger than 64 characters, otherwise certmanager will fail to create SSL cert
  # https://support.cpanel.net/hc/en-us/articles/4405807056023-Let-s-Encrypt-NewOrder-request-did-not-include-a-SAN-short-enough-to-fit-in-CN-
  normalized_env_prefix = "gke-${var.env_prefix}"
  tags                  = ["gke-firewall-rule", "${local.normalized_env_prefix}-firewall-rule"]
  // The labels that will be added to each instance so they can be better classified
  labels = merge(var.labels, {
    project = var.gl_project_title
  })
}
