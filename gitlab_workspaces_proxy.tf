resource "helm_release" "gitlab_workspaces_proxy" {
    name  = var.workspaces_proxy_name
    repository = "https://gitlab.com/api/v4/projects/gitlab-org%2fremote-development%2fgitlab-workspaces-proxy/packages/helm/devel"
    chart      = "gitlab-workspaces-proxy"
    namespace = "gitlab-workspaces"
    create_namespace = true
    version = var.workspaces_proxy_helm_chart_version

    set {
      name = "service.ssh.loadBalancerIP"
      value = google_compute_address.ssh_ip_address.address
    }

    set {
      name = "auth.client_id"
      value = var.workspaces_proxy_client_id
    }

    set {
      name = "auth.client_secret"
      value = var.workspaces_proxy_client_secret
    }

    set {
      name = "auth.host"
      value = var.workspaces_proxy_auth_host
    }

    set {
      name = "auth.redirect_uri"
      value = var.workspaces_proxy_redirect_uri
    }

    set {
      name = "auth.signing_key"
      value = var.workspaces_proxy_signing_key
    }

    set {
      name = "ingress.className"
      value = var.workspaces_proxy_ingress_class_name
    }

    set {
      name = "ingress.tls.workspaceDomainCert"
      value = var.workspaces_proxy_workspace_domain_cert
    }

    set {
      name = "ingress.tls.workspaceDomainKey"
      value = var.workspaces_proxy_workspace_domain_key
    }

    set {
      name = "ingress.tls.wildcardDomainCert"
      value = var.workspaces_proxy_wildcard_domain_cert
    }

    set {
      name = "ingress.tls.wildcardDomainKey"
      value = var.workspaces_proxy_wildcard_domain_key
    }

    set {
      name = "ingress.host.workspaceDomain"
      value = var.workspaces_proxy_ingress_host_workspace_domain
    }

    set {
      name = "ingress.host.wildcardDomain"
      value = var.workspaces_proxy_ingress_host_wildcard_domain
    }

    set {
      name = "ssh.host_key"
      value = var.workspaces_proxy_ssh_host_key
    }

    set {
      name = "log_level"
      value = var.workspaces_proxy_log_level
    }

    depends_on = [
        module.gke
    ]
}
