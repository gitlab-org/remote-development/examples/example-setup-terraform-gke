output "ingress_ip_address" {
    value = google_compute_address.ingress_ip_address.address
}

output "ssh_ip_address" {
    value = google_compute_address.ssh_ip_address.address
}